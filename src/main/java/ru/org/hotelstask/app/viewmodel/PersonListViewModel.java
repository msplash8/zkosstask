package ru.org.hotelstask.app.viewmodel;

import org.zkoss.bind.annotation.Init;
import org.zkoss.zul.ListModelList;
import ru.org.hotelstask.app.ejb.PersonLocalBean;
import ru.org.hotelstask.app.entity.Person;
import ru.org.hotelstask.app.model.PersonModel;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;

public class PersonListViewModel {
    private ListModelList<PersonModel> personListModel;

    @Init // @Init annotates a initial method
    public void init(){
        //get data from service and wrap it to model for the view
        PersonLocalBean personLocalBean = null;
        try {
            InitialContext ic = new InitialContext();
            personLocalBean = (PersonLocalBean) ic.lookup("java:comp/env/personLocalBean");
        } catch (NamingException ex) {
            // log ex
        }
        List<PersonModel> personList = personLocalBean.getAllPersons();
        //you can use List directly, however use ListModelList provide efficient control in MVVM
        personListModel = new ListModelList<PersonModel>(personList);
    }

    public ListModelList<PersonModel> getPersonListModel() {
        return personListModel;
    }
}
