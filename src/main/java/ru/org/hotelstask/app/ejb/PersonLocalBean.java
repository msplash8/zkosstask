package ru.org.hotelstask.app.ejb;

import ru.org.hotelstask.app.converter.PersonConverter;
import ru.org.hotelstask.app.dao.PersonDAO;
import ru.org.hotelstask.app.entity.Person;
import ru.org.hotelstask.app.model.PersonModel;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class PersonLocalBean {
    @Inject
    private PersonDAO personDAO;


    public List<PersonModel> getAllPersons() {
        List<Person> personList = personDAO.getAllPersons();
        List<PersonModel> personModels = new ArrayList<>();
        for (Person person : personList) {
            PersonModel personModel = new PersonModel();
            PersonConverter.convertFromEntityToModel(person, personModel);
            personModels.add(personModel);
        }
        return personModels;
    }
    public boolean savePerson(PersonModel personModel) {
        Person person = new Person();
        PersonConverter.convertFromModelToEntity(personModel, person);
        return personDAO.savePerson(person);
    }

}