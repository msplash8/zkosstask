package ru.org.hotelstask.app.dao;

import ru.org.hotelstask.app.entity.Person;

import java.util.List;

public interface PersonDAO {
     List<Person> getAllPersons();
     boolean savePerson(Person person);

}
