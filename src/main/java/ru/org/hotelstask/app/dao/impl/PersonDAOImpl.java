package ru.org.hotelstask.app.dao.impl;

import ru.org.hotelstask.app.dao.PersonDAO;
import ru.org.hotelstask.app.entity.Person;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
// Actually do we need DAO ---------
@Stateless
public class PersonDAOImpl implements PersonDAO {
    @PersistenceContext(unitName = "manager1")
    private EntityManager em;

    @Override
    public List<Person> getAllPersons() {
        List personList = em.createQuery(
                "select person from Person as person")
                .getResultList();
        return personList;
    }

    @Override
    public boolean savePerson(Person person) {
        em.persist(person);
        return true;
    }
}
