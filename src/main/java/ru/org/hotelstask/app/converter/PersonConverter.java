package ru.org.hotelstask.app.converter;

import ru.org.hotelstask.app.entity.Person;
import ru.org.hotelstask.app.model.PersonModel;

public class PersonConverter {
    public static void convertFromModelToEntity(PersonModel personModel, Person person) {
        person.setFirstName(personModel.getFirstName());
        person.setLastName(personModel.getLastName());
        person.setMiddleName(personModel.getMiddleName());
    }
    public static void convertFromEntityToModel(Person person, PersonModel personModel) {
        personModel.setFirstName(person.getFirstName());
        personModel.setLastName(person.getLastName());
        personModel.setMiddleName(person.getMiddleName());
    }
}
