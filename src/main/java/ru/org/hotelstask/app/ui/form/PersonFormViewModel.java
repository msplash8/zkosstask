package ru.org.hotelstask.app.ui.form;

import org.zkoss.bind.annotation.Command;
import ru.org.hotelstask.app.ejb.PersonLocalBean;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class PersonFormViewModel extends PersonForm {
    @Command
    public void submit() {
        PersonLocalBean personLocalBean = null;
        try {
            InitialContext ic = new InitialContext();
            personLocalBean = (PersonLocalBean) ic.lookup("java:comp/env/personLocalBean");
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
        personLocalBean.savePerson(super.getPersonModel());
        System.out.println("Submitted from form");
    }
}
