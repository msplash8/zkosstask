package ru.org.hotelstask.app.ui.form;

import ru.org.hotelstask.app.entity.Person;
import ru.org.hotelstask.app.model.PersonModel;

public class PersonForm {
    private PersonModel personModel = new PersonModel();

    public PersonModel getPersonModel() {
        return personModel;
    }

    public void setPerson(PersonModel person) {
        this.personModel = person;
    }
}
