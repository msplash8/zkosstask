package ru.org.hotelstask.app.ui.dialog;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

public class AddPersonDialogController extends SelectorComposer<Component> {
    private static final long serialVersionUID = 1L;

    @Wire
    Window modalDialog;

    @Listen("onClick = #submitBtn")
    public void showModal(Event e) {
        System.out.println("Submitted");
        modalDialog.detach();
    }
}